Project Proposal for EL6483
==========================

## Team

Team members (up to four). Write name and email address of each team member:


1. Matthew Izberskiy (mi737@nyu.edu)

2. Sarvagya Gupta (sg3483@nyu.edu)

3.

4.

## Idea

Describe your project idea, in 1-2 paragraphs. Also explain your motivation for this project idea - why is this project interesting to you?

The project idea that we are going with is a smart outlet. This outlet will be able to be controlled over bluetooth. The outlet is going to use some form of authentication, probably password based, in order to allow or disallow certain people from using it.

----
The smart outlet will not use 120V AC lines. The smart outlet will simulate the 120V AC lines with a 5V 2A DC line. This is done for safety reasons.

----

The reason this project is interesting to us is becaue smart appliances and smart homes are becoming the norm. Smart outlets are a part of the smart home and the chance to make a functioning smart outlet seems like a really cool and useful idea to us.

## Materials

The STM32F4 Discovery board should be the primary microcontroller in your project. However, you may use other devices (e.g. a secondary microcontroller or external peripherals) if you need to (based on your project requirements).

If you plan to use any extra hardware, list each device and explain what it will be used for. If you need to buy a device, include a link to the webpage for the product you intend to purchase. If you already own a device, link to the webpage from which you purchased it.

Bluetooth chip - http://www.amazon.com/KEDSUM%C2%AE-Arduino-Wireless-Bluetooth-Transceiver/dp/B0093XAV4U/ref=sr_1_1?ie=UTF8&qid=1426740185&sr=8-1&keywords=bluetooth+chip

Relay - http://www.amazon.com/gp/product/B0057OC6D8/ref=ox_sc_act_title_3?ie=UTF8&psc=1&smid=ATVPDKIKX0DER

Extension Cord - http://www.amazon.com/gp/product/B000V1LX0O/ref=gno_cart_title_1?ie=UTF8&psc=1&smid=A27DUHPPTQX1VZ

Powered USB HUB -
http://www.amazon.com/Rosewill-4-Port-Power-Adapter-RHUB-210/dp/B00552PMN8/ref=sr_1_21?s=pc&ie=UTF8&qid=1427337211&sr=1-21&keywords=powered+usb+hub

USB Male to Female Cable -
http://www.amazon.com/AmazonBasics-Extension-Cable--Male--Female/dp/B00B3P1IGG/ref=sr_1_5?s=electronics&ie=UTF8&qid=1427337303&sr=1-5&keywords=usb+male+to+female+cable

Current Measurement IC (if power reading will be done) -

http://www.mouser.com/ProductDetail/STMicroelectronics/STPM01FTR/?qs=7bo3WR6gf9RuedNNpQDZ9g%3D%3D&kpid=11577733&gclid=CNXz56P-xMQCFYk8gQodNHoAHw

## Milestones

Milestones mark specific points along a project timeline. Your project should have two milestones, and you should plan to demonstrate a working prototype at each:

* 16 April 2015: At this stage, you should have a preliminary working prototype of *something*. It may not have all the functionality of your final project, but you should have something operational to show at this point. Describe what you will demonstrate during this week.

Switchable relay using bluetooth. It will probably not be a very smart outlet at this stage, but it will work over bluetooth.

* 7 May 2015: During finals week, you will demonstrate your completed project. What do you intend to demonstrate at this time? What features will your final project have?

We will demonstrate a fully functioning smart outlet. It will have the ability to time out usage based on a set time allotment (mimicing commercial usage). It will also authenticate users. Depending on the difficulty and the available time to us, we will also be using a current clamp to measure power usage and we will report that to the user via bluetooth or an lcd dsiplay.

## Plan of work

 * Describe (in detail) how you plan to divide the work for your project among the team members. This is a group project, and you are all responsible for ensuring the project's successful outcome; but I want to see how you plan to divide the work amongst yourselves. Who is going to be responsible for each part of the project? Make sure to divide the work in a reasonable way.

 Sourcing the parts - Matthew

 Writing code for bluetooth chip - Matthew

 Writing Relay Code - Gupta

 Wiring - Matthew

 Authentication Code- Gupta

 User Interface - Matthew/Gupta

 Bug Fixing - Matthew/Gupta

 Power Reading (if done) - Matthew/Gupta

 Quality Assurance for power reading (if done)- Matthew

 Report - Gupta


 * You will have roughly 6 weeks to work on this project. Describe what each team member needs to accomplish in each week, and what the group as a whole needs to accomplish in each week. (Plan ahead; for example, if you need to purchase extra materials, you may need to wait a couple of weeks for those to arrive.) Indicate how many hours of work is expected from each team member in each week - be realistic.

 #####Below are rough estimates of time needed:

 Sourcing parts - 2 hour

 Part Arrival - 4~5 days

 Bluetooth code - 6 hours

 Relay Code - 4 hours

 Wiring - 4 hours

 #####Milestone 1

 Authentication code - 6 hours

 User Interface - 4 hours

 Bug Fixing - 3 hours

 Power Reading(if done) - 8 hours

 Quality Assurance - 4 hours

 Report - 4 hours

 #####Final presentation
