Project Report for the SMART OUTLET
==========================

## Team

Team members (up to four). Write name and email address of each team member:


1. Matthew Izberskiy (mi737@nyu.edu)

2. Sarvagya Gupta (sg3483@nyu.edu)


## Abstract

This project simulates the behavior of a commercial smart outlet. A system similar to the one we created can be put in
a public places and it can charge users for the electricity they use. The system we built can only limit users on time, however a power meter can be incorporated into this design to provide data on power use and charge users per kWh. This project uses an STM32F4 as the controller, a relay board, and a bluetooth chip. Communication with our system can be done through any bluetooth terminal app.


## Introduction

Smart devices are becoming more and more common in our daily lives. Everything form our TVs to our refrigerators are becoming smart. Smart devices open up a world of possibilities that just were not possible or wer very expensive prior.

One such device is the smart outlet. There are currently many smart outlets out on the market. One of their main utilities is the ability to turn the outlet on and off remotely. This is a fantastic function of the smart outlets and it is useful in many cases. One such use is the ability to turn an air conditioner on and off remotely. For example, if it is a hot summer day, a person would like to have their house be cool by the time they come back home from work. Leaving the air conditioner on all day when there is no one home is extremely wasteful. However, if the person can turn the air conditioner on 30 minutes before they get home from work, the house will be cooled and the air conditioner wouldn't have been running all day.

The reason we decided to make the smart outlet our project is because we think it is one of the most useful smart devices a person can currently buy. A lot of the smart outlets on the market are currently fairly expensive, $80 or more. We wanted to see if it was possible to build a smart outlet that would be cheaper.


## The hardware

STM32F4 Discovery- This is the micro controller that we used for our smart outlet. This can be found at many online retailers for around $15. A link to one such retailer is below.
[STM32F4 discovery](http://www.newark.com/stmicroelectronics/stm32f4discovery/stm32-f4-series-discovery-kit/dp/87T3791?mckv=squPUGuNd|pcrid|57087229341|plid|&CMP=KNC-GPLA?gross_price=)

Bluetooth Chip (HC-06) - We decided to use bluetooth communications for our smart outlet. Bluetooth is easy set up and use and just about every phone has a bluetooth transiever. We used an HC-06 chip, but an HC-05 chip should work just as well. It doesn't matter what manufactuer you go with, just get one that has good reviews so that you know you are getting a good working product. The particular chip we used is below:

[bluetooth](http://www.amazon.com/KEDSUM%C2%AE-Arduino-Wireless-Bluetooth-Transceiver/dp/B0093XAV4U/ref=sr_1_1?ie=UTF8&qid=1426740185&sr=8-1&keywords=bluetooth+chip)


Relay - The relay is a crucial part of the smart outlet. The relay is what lets us switch an outlet on and off. You can use any relay you want. You don't even need to get a relay board. However, we recommend getting a relay board because they don't require any assembly and they only need a few pin connections to operate. Originally, we decided to go with a two channel relay board that used songle relays but then decided to switch to a 4 channel relay board that uses songle relays. For reasons not entirely known to us, the two channel board required us to use a ULN2803 current driver IC to operate it since the STM32F4's GPIO pins are only 3.3v, while the 4 channel relay board did not require a current driver IC. We are not sure why it works without a current driver since both boards were from the same manufacturer and they both use the same relays, but it does. The 4 channel relay board that we used is below, but feel free to use any relay board but beware that you may need a ULN2803 or other current driver IC to operate it.

[relay](http://www.amazon.com/SunFounder-Channel-Shield-Arduino-Raspberry/dp/B00E0NSORY/ref=sr_1_3?ie=UTF8&qid=1431062609&sr=8-3&keywords=4+channel+relay)

#####The parts below are specific to our project because we are not going to operate with live wires, we are simply going to simulate a smart outlet using a 5v 2A line. If you want to use live wires, these parts will not be necessary.

USB Hub - This hub will act as a bridge from the unsafe live 120V AC wires to the much safer 5V 2A DC wires. The hub is where we are going to get the line that we will run through the relay. The line will be a standard USB extension cable. The hub that we used is linked below

[hub](http://www.amazon.com/Rosewill-4-Port-Power-Adapter-RHUB-210/dp/B00552PMN8/ref=sr_1_21?s=pc&ie=UTF8&qid=1427337211&sr=1-21&keywords=powered+usb+hub)


USB Extension Cable - This part gets a little tricky because it needs a bit of manual labor to make the cable useful to us. The first step is to carefully cut away the rubber coating from the wire from the USB cable at around the half lenghh of the cable going abour 3 inches in both directions. Your cable may or may not be a shielded cable. If it is a shielded cable, you are going to have to cut away the metalic coating. The shielding differs greatly from cable to cable to you'll have to figure out on your own how to remove that shielding but it should be an easy task. At this point, you should see either 4 or 5 wires. Only one of these wires is of interest to us and it is almost always red in color. If there is no red wire, you might have to try to get a different USB cable to operate on. Once you locate the red wire, take a pair of wire cutters and cut it in half. Strip away about a thumb's worth of rubber coating on each half. These two stripped ends will run through the relay. A link to the USB extension cable we used is below.

[cable](http://www.amazon.com/AmazonBasics-Extension-Cable--Male--Female/dp/B00B3P1IGG/ref=sr_1_5?s=electronics&ie=UTF8&qid=1427337303&sr=1-5&keywords=usb+male+to+female+cable)

##### Connections

| Pin On Board | Device/Pin on Device | Purpose                                                              |
| ------------ | -------------------- | -------------------------------------------------------------------- |
| PA2          | 4 Channel Relay/ In1 | GPIO output from board to the input on the relay for the first relay |
| 5V           | 4 Channel Relay/VCC  | 5v from board to supply power to Relay                               |
| GND          | 4 Channel Relay/GND  | GND from board to GND on Relay                                       |
| PB6          | Bluetooth HC06/RX    | TX from board to RX on bluetooth                                     |
| PB7          | Bluetooth HC06/TX    | RX from board to TX on bluetooth                                     |
| 3V           | Bluetooth HC06/VCC   | 3v from board to supply power to bluetooth chip                      |
| GND          | Bluetooth HC06/GND   | GND from board to GND on bluetooth chip                              |

![](http://i.imgur.com/TqAo4wG.jpg)

## Software

This project uses the standard UART and the standard GPIO libraries.

Information about the UART library can be found here:
[uart](http://stm32f4-discovery.com/2014/04/library-04-connect-stm32f429-discovery-to-computer-with-usart/)

Information about the GPIO library can be found here: [gpio](http://stm32f4-discovery.com/2014/04/stm32f429-discovery-gpio-tutorial-with-onboard-leds-and-button/)

This program is broken up into two main components: the main loop and the USART handler.

The main loop acts as a state machine and reacts to the states set by the USART handler. The mainloop can also set states and setting the state to "logged off" affects the USART Handler's actions.

The USART handler has two separate functionalities; one for when the user is not logged in and one for when the user is logged in. When the user is not logged in, the USART handler sends the bytes it receives to a password checker. When the user is logged in, it sets the global state depending on the bytes it receives.

To see more detailed information about the software portion of this project, check out the main.c file in the src folder. All the code is documented and should provide more insight into the functionality.


##Instruction For Using This Project
To use this project, the user must assemble the parts as shown in the connection diagram. The user must then flash ("make flash") this code to their board and restart it. However, for better visibility for what is happening behind the seasons, it is recommended that the user run this code by executing the commands "make debug" and "make openocd."

The user must also have a bluetooth terminal either on their phone or computer.

Once the board is initialized and the user is connected to the bluetooth chip using the bluetooth terminal, the user can then enter their password.

Currently, the only two passwords in the system are "matt" and "fraida"

The password must be entered one letter at a time. Once all letters are entered, the "*" symbol must be sent. If all the letters were sent properly, the user will now be logged in. If the letters were not sent properly, the user must repeat the process of entering a password again.

Once logged in, the user can send a "1" to turn the outlet on. Once the outlet is on, the user can also send a "t" to see how much time is left on the account. Once the time runs out, the outlet will turn off on its own. The user can also send an "l" to log off. Once the user is logged off, the user can log on again to either account by entering the password again.

By default, "matt" has 600 seconds on the account while "fraida" has 10 seconds on the account.



##Discussion

The first step in this project, once the idea was finalized, was to source the parts. We wanted to go with parts that were cheap and well documented. Most of the relays that we found were all well documented by people who have used them and posted their own guides online. Any relay we found would have worked, but we went with the 4 channel relay simply because it had a lot of good reviews on Amazon and it would arrive within two days.

We had similar parameters for selecting the bluetooth chip. We did not have any prior knowledge as to which bluetooth chips would work well so once again, we relied on the amazon reviews.The specific bluetooth chip we selected had a lot of good reviews and the reviews pointed out that it was very easy to use and it had a simple UART interface.

To decide how we wanted to simulate the functionality of a full smart outlet, we thought about what would be a useful alternative to a full 120V outlet. We decided that a USB charger was a good substitute since it is very ubiquitous and many people use one everyday.

To create a USB charger, we needed a power source for the USB. We decided that a powered USB hub would be the easiest solution. However, we still needed a way to be able to interrupt the USB cable with the relay. For that, the only way we could think of was to make two incisions in the USB cable, strip away the rubber and shielding, and then cut the 5v line and run it through the relay.

Initially, one of the problems we faced was that the STM32F4 was unable to energize the relay coil because its GPIO output was limited to 3V. As a solution, we purchased an ULN2308 current driver and wired the GPIO through the current driver and fed the ULN2308 with an outside 5v source. However, we had another 4 channel relay (the one in the amazon link) on chance and we decided to try it out. For a reason unknown to us, the other relay's coil was able to be energized with the output from the GPIO of the STM32F4 without the need for a ULN2308 current driver. Because of this, we decided to ditch the first relay we bought and just used the second one for simplicity.

For the software portion of this project, we decided to go with a finite state machine design. The reason for this is because the smart outlet essentially functioned in just a few distinct states which meant that coding a finite state machine for this design would be both, simple and effective.

As always with coding, we had a lot of bugs in the code. Namely, we found that we were only able to log in once and the time would display only once. After some time debugging, we found that the issues were mostly in the state design, so we had to modify our original state design.

The biggest problem we had was the inability of our smart outlet to receive more than two bytes at a time. We believe the issue is that the bluetooth terminal on the phone sends the bytes with too little of a delay in between bytes. We couldn't find a way to increase the delay in the bluetooth terminal, so instead, we decided to only send one byte at a time and then terminate it with a "*" symbol to tell the board that the user is done entering password.

##Results

At the end of the project, the smart outlet functioned as it was supposed to.

## Conclusion

What we found is that it is very important to source the parts well and to read what the exact specifications are. With the first relay, we failed to realize that the board's GPIO output was insufficient to energize the relay coil so we had to waste time and wait for the ULN2308 chip to arrive. If we did more research prior, we would not have wasted several days not being able to progress while waiting for the part.

Overall, the project was a success. Smart outlets are useful for many purposes. A design like our project can be used in a kiosk where the owner of the kiosk can charge users for using the outlets on a time basis. Additionally, it will be relatively simple to incorporate power metering functionality into this project so that the power use can also be seen. That will be something we will do when we are refining the project on our own time.  
