#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void init();
void loop();

void initLeds();
void initButton();


int main() {
    init();

    do {
        loop();
    } while (1);
}

void init() {
    initLeds();
    initButton();
}

void loop() {

    if ((GPIOA->IDR & 0x00000001)==1) {
//1011 1100 0000 1111 0100 1111 0000 1011
//0000 0000 0000 0000 0000 0000 0000 0001
//0000 0000 0000 0000 0000 0000 0000 0001
//0000 1111 0000 1111 1010 10x1 0011 1001
//0000 0000 0000 0000 0000 0010 0000 0000
//0000 0000 0000 0000 0000 00x0 0000 0000 >>9
//0000 0000 0000 0000 0000 0000 0000 000x 
        // Use BSRRL, set bits of GPIOD pins 12-15 and the port D pin 0
	GPIOD->BSRRL|=0xF001;
	//0000 0000 0000 0000 1111 0000 0000 0001
	//Set the bit of GPIO port E.
	//GPIOE->BSRRL|=0x0001; 	
    }
    else {
        // Use BSRRH, set bits of GPIOD pins 12-15 and port D pin 0
	GPIOD->BSRRH|=0xF001;
	//Set (reset) the value of port E
	//GPIOE->BSRRH|=0x0001;
    }
}

void initLeds() {
    // Enable GPIOD Clock
    RCC->AHB1ENR|=0x08;
    // Set GPIOD pins 12-15 mode to output and port D pin 0
    GPIOD->MODER|=0x55000001;
    //0101 0101 0000 0000 0000 0000 0000 0001

}

void initButton() {
    // Enable GPIOA Clock
    RCC->AHB1ENR|=0x01;
    // Set GPIOA pin 0 mode to input
    GPIOA->MODER&=~(3);
	
	//Enable Pin E Port 0 to OUTPUT
	//GPIOE->MODER|=(1);
}

