#include <misc.h>
#include "stdio.h"
#include "stdlib.h"
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_conf.h>
//#include <tm_stm32f4_gpio.h>
//#include <tm_stm32f4_usart.h>
#include <stm32f4xx_usart.h>
#define MAX_STRLEN 100 // this is the maximum string length of our string in characters
volatile char received_string[MAX_STRLEN+1]; // this will hold the recieved string

void SysTick_Handler(void);
uint8_t loggedIn = 0;  //global variable that shows if anyone is logged in or not
uint8_t loggedIndex = 0; //global variable that shows the index of the person logged in
uint8_t status = 0;  //0 for off, 1 for on, 2 for displaying time left, 3 for logging off


uint8_t Buffer[6];  //depreciated


volatile uint32_t msTicks; // Counts 1ms timeTicks
const uint16_t LEDS = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;

struct user   //struct that holds the user data. It has their password and the amount of time they have left
{
	char *password;
	float time;

};
struct user users[2];  //array of user structs. Currently, there are only two users

struct returnType  //struct that is used as the return type of the comparePassword function
{
	uint8_t found;
	uint8_t index;
};


const uint32_t baudrate = 9600;

void clearString(void)  //function that clears the string that is used in the USART IRQ handler
{
	int i = 0;
	for (i;i<MAX_STRLEN+2;i++)
	{
		received_string[i]=NULL;
	}
}

void InitSystick(void){
	SystemCoreClockUpdate();                      /* Get Core Clock Frequency   */
  if (SysTick_Config(SystemCoreClock / 1000)) { /* SysTick 1 msec interrupts  */
    while (1);                                  /* Capture error              */
  }
}

void SysTick_Handler (void)
{
  msTicks++;                                    // increment Delay()-counter
}




void Delay(__IO uint32_t nCount) {
  while(nCount--) {
  }
}

/* This funcion initializes the USART1 peripheral
 *
 * Arguments: baudrate --> the baudrate at which the USART is
 * 						   supposed to operate
 */
void init_USART1(uint32_t baudrate){     //borrowed from an online source, but also very similar to what we have done. I just coppied it to save time.

	/* This is a concept that has to do with the libraries provided by ST
	 * to make development easier the have made up something similar to
	 * classes, called TypeDefs, which actually just define the common
	 * parameters that every peripheral needs to work correctly
	 *
	 * They make our life easier because we don't have to mess around with
	 * the low level stuff of setting bits in the correct registers
	 */
	GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct; // this is for the USART1 initilization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PB6 for TX and PB7 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7; // Pins 6 (TX) and 7 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOB, &GPIO_InitStruct);					// now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1); //
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;// we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;		// we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;		// we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; // we want to enable the transmitter and the receiver
	USART_Init(USART1, &USART_InitStruct);					// again all the properties are passed to the USART_Init function which takes care of all the bit setting


	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART1, ENABLE);
}

/* This function is used to transmit a string of characters via
 * the USART specified in USARTx.
 *
 * It takes two arguments: USARTx --> can be any of the USARTs e.g. USART1, USART2 etc.
 * 						   (volatile) char *s is the string you want to send
 *
 * Note: The string has to be passed to the function as a pointer because
 * 		 the compiler doesn't know the 'string' data type. In standard
 * 		 C a string is just an array of characters
 *
 * Note 2: At the moment it takes a volatile char because the received_string variable
 * 		   declared as volatile char --> otherwise the compiler will spit out warnings
 * */
void USART_puts(USART_TypeDef* USARTx, volatile char *s){  //function that sends over the USART connection (bluetooth)

	while(*s){
		// wait until data register is empty
		while( !(USARTx->SR & 0x00000040) );
		USART_SendData(USARTx, *s);
		*s++;
	}
}

void initRelay() {  //function that initializes the relay pins. I use PA2 as the pin for the relay
    // Use RCC_AHB1PeriphClockCmd to enable the clock on GPIOD
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    // Declare a gpio as a GPIO_InitTypeDef:
    GPIO_InitTypeDef gpio;

    // Call GPIO_StructInit, passing a pointer to gpio
    // This resets the GPIO port to its default values
    GPIO_StructInit (&gpio);

    // Before calling GPIO_Init,
    // Use gpio.GPIO_Pin to set the pins you are interested in
    // (you can use the LEDS constant defined above)
    gpio.GPIO_Pin = GPIO_Pin_2;
    // Use gpio.GPIO_Mode to set these pins to output mode
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    // Now call GPIO_Init with the correct arguments
    GPIO_Init(GPIOA, &gpio);


}

struct returnType comparePassword(char* string,uint8_t count)  //function that compares the passed in password to one of the stored ones. This function must also be passed in the size of the string that we are interested in
{
	struct returnType temp;
	temp.found = 0;
	printf("the passed in string is %s\n",string);
	printf("checking that string against %s\n",users[1].password);
	printf("checking that string against %s\n",users[0].password);
	uint8_t hits[2] = {0,0};
	for (uint8_t i = 0; i<2;i++)
	{

		//printf("outside for loop ran\n");
		for (uint8_t j = 0; j< count; j++)
		{

			//printf("for loop ran\n");
			printf("comparing %c to %c\n",users[i].password[j],string[j]);
			if (users[i].password[j] == string[j])
			{
				//printf("comparing %c to %c\n",users[i].password[j],string[j]);
				hits[i]+=1;

			}
		}
	}
	for (uint8_t i = 0;i<2;++i)
	{
		printf("the number of hits is %d\n",hits[i]);
		printf("the count is %d\n",count);
		//printf( )
		//if (hits[i] == count && sizeof(users[i].password)/sizeof(char)==count)
		if (hits[i] == count)
		{
			printf("validated son\n");
			temp.index =i;
			temp.found =1;
		}
	}


	printf("found: %d\n",temp.found);
	return temp;
}

void relayControl(uint8_t status)  //function that either sets or resets the PA2 pin for the relay. 1 is for off, 0 is for on. The relay is running on reverse logic
{
	if (status)
	{
		printf("turning relay off \n");
		GPIO_SetBits(GPIOA,GPIO_Pin_2);
	}
	else
	{
		printf("turning relay on\n");
		GPIO_ResetBits(GPIOA,GPIO_Pin_2);
	}
}


// this is the interrupt request handler (IRQ) for ALL USART1 interrupts
void USART1_IRQHandler(void){  //this is the USART IRQ handler function. This handles all the data that the bluetooth chip receives.

	// check if the USART1 receive interrupt flag was set
	char tcopy = 'a'; //dummy char
	//char tcopy[16];
	while( USART_GetITStatus(USART1, USART_IT_RXNE) ){

		static uint8_t cnt = 0; // this counter is used to determine the string length

		char t = USART1->DR; // the character from the USART1 data register is saved in t
		tcopy = t;
		/* check if the received character is not the LF character (used to determine end of string)
		 * or the if the maximum string length has been been reached
		 */
		if( (t != '*') && (cnt < MAX_STRLEN) ){     //the turnminating character is '*' The string lenght should never reach the max_strlen under normal use. If it does, it might requre the user the send a '*' again to reset things.
			printf("got terminating character\n");
			received_string[cnt] = t;
			cnt++;
		}

		else if (!loggedIn)  //action that happens once the user sends a terminating character. It sends the passed in string and the current count to the password checker. and then sets the proper states.
		{ // otherwise reset the character counter and print the received string
			//printf("%s\n",received_string);
			struct returnType temp = comparePassword(received_string,cnt);
			printf("going to check your password\n");
			if (temp.found)   //if the password matches, the code below is run
			{
				loggedIn = 1;
				loggedIndex = temp.index;
				printf("Congrats, you logged in\n");
				cnt = 0;

			}
			else  //if it isn't the code below is run.
			{
				printf("wrong password bro\n");
				//clearString();
				cnt = 0;
			}

			//USART_puts(USART1, received_string);
		}
		printf("received string is: %s\n", received_string);  //these were used during debugging and have no further use
		printf("The character is: %c\n", t);
		Delay(100000);
		if (loggedIn)  //this behavior takes place once the user is successfully logged in. The only significant inputs are 1,t and l. 0 was used for debugging. 1 turns on the relay and starts keeping track of how much time was used.
		//t displays how much time is left over bluetooth
		//l logs off and shows how much time is left.
		{


			if (t == '0')
			{

				status = 0;
			}
			else if (t == '1')
			{

				status = 1;
			}
			else if (t == 't')
			{
				status = 2;
			}
			else if (t == 'l')
			{
				status = 3;
				cnt = 0;
			}
		}



	}
	printf("%s\n",received_string);
}

int main(void)  //main function, this is where the magic happens
{
	uint32_t curTicks = msTicks;
	SystemInit();
	InitSystick();
	initRelay();

	printf("this is running\n");
	init_USART1(9600); // initialize USART1 @ 9600 baud
	printf("usart was started\n");
  	USART_puts(USART1, "Init complete! Hello World!\r\n"); // just send a message to indicate that it works
  	printf("sent initial message\n");
  	users[0].password = "matt";  //these set up the user accounts. I get 10 minutes of time, Fraida gets 10 seconds because she only gets to a demo account. Plus I want to demonstrate how the action of when the time runs out
  	users[0].time=600;
  	users[1].password = "fraida";
  	users[1].time = 10;
  	float startTime= 0;  //this variable is for time keeping
  	uint8_t lastStatus = 0; //dummy last status
  	relayControl(1);
  	//uint8_t check = 1;
  while (1)  //below are all the states. Think of it like a finite state machine.
  {
  	//printf("test\n");
  	if (status ==1 && lastStatus == 0 && loggedIn)  //this state happens when there wasn't anyone logged in before and the user logs in
  	{
  		startTime =msTicks;
  		relayControl(0);
  		lastStatus = 1;
  		//printf("in status 1\n");
  	}
    else if (status == 1 && lastStatus == 1 && loggedIn) //this statement happens when the user is logged in and continues in the normal operation.
    {
    	if (((msTicks -startTime)/1000)>users[loggedIndex].time)  //this action happens if the user's time runs out
    	{
    		loggedIn = 0;
    		status = 0;
    		relayControl(1);
    		users[loggedIndex].time = 0;
    		USART_puts(USART1, "You have run out of time, you have been logged out\r\n");
    		//printf("also in status 1\n");
    	}
    	else  //if the user's time hasn't run out, nothing significant happens
    	{
    		lastStatus=1;
    		//printf("in this little statement\n");
    	}
    }
    else if(status == 2 &&loggedIn)  //this state is for displaying how much time is left
    {
    	float timeLeft = users[loggedIndex].time-(((msTicks-startTime)/1000));
    	lastStatus=1;
    	char temp[15];
    	sprintf(temp,"%f",timeLeft);
    	USART_puts(USART1,temp);
    	USART_puts(USART1," seconds left\r\n");
    	status=1;
    	//printf("in status 2\n");
    }
    else if (status ==3 &&lastStatus !=3 &&loggedIn)  //this statement is for logging out. It also does an update of the users remaining time. 
    {
    	loggedIn=0;
    	lastStatus = 0;
    	status=0;
    	users[loggedIndex].time = users[loggedIndex].time-(((msTicks-startTime)/1000));
    	relayControl(1);
    	//clearString();
    	char temp[15];
    	sprintf(temp,"%f",users[loggedIndex].time);
    	USART_puts(USART1,"You have logged out\r\n");
    	USART_puts(USART1, "You have ");
    	USART_puts(USART1,temp);
    	USART_puts(USART1," seconds left\r\n");
    	//printf("in status 3\n");
    }
  }
}
